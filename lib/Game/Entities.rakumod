unit class Game::Entities:ver<0.1.7>:auth<zef:jjatria>;

# The main entity registry, inspired by https://github.com/skypjack/entt

my constant SPARSE     = 0;
my constant DENSE      = 1;
my constant COMPONENTS = 2;

# Entity GUIDs are 32 bit integers:
# * 12 bits used for the entity version (used for recycing entities)
# * 20 bits used for the entity number
my constant ENTITY-MASK  = 0xFFFFF; # Used to convert GUIDs to entity numbers
my constant VERSION-MASK = 0xFFF;   # Used to convert GUIDs to entity versions
my constant ENTITY-SHIFT = 20;      # The size of the entity number within a GUID
my constant NULL-ENTITY  = 0;       # The null entity

# Keys in this hash are component type names (ie. the result of .^name),
# and values are sparse sets of entities that "have" that component.
has %!components{Mu};

# Parameters used for recycling entity GUIDs
# See https://skypjack.github.io/2019-05-06-ecs-baf-part-3
has @!entities = (Mu);
has $!available is default(NULL-ENTITY);

has %!view-cache;

# Indexing on the component registry is done using type objects
# If we get a role, we use its punned class in order to avoid
# missing reads later.
my &type = -> $_ { .HOW ~~ Metamodel::ClassHOW ?? .WHAT !! .^pun }

# Entity "methods"
my &version = { $^entity +> ENTITY-SHIFT }
my &entity  = { $^entity +& ENTITY-MASK  }
my &is-null = { .&entity == NULL-ENTITY  }
my &format  = { .&version.fmt('%03X') ~ ':' ~ .&entity.fmt('%05X') }

# Set "methods"
my &contains = -> $set, $index {
    try ( $set[ DENSE; $set[ SPARSE; $index ] // Int ] // $index + 1 ) == $index;
}

my &swap-components = -> $set, $le, $re {
    my ( $ld, $rd ) = $set[ SPARSE; $le, $re ];
    $set[ COMPONENTS; $ld, $rd ] = $set[ COMPONENTS; $rd, $ld ];
};

my &swap = -> $set, $le, $re {
    my ( $ld, $rd ) = $set[ SPARSE; $le, $re ];
    my ( $ls, $rs ) = $set[ DENSE;  $ld, $rd ];

    die "Cannot swap $le and $re: they are not members of the set"
        unless $ls == $le && $rs == $re;

    $set.&swap-components( $le, $re );
    $set[ DENSE;  $ld, $rd ] = $set[ DENSE;  $rd, $ld ];
    $set[ SPARSE; $ls, $rs ] = $set[ SPARSE; $rs, $ls ];
};

method !add-version ($e) { $e +| ( ( @!entities[$e] // 0 ).&version +< ENTITY-SHIFT ) }

method created ( --> Int ) { @!entities.elems - 1 }

# Get the number of created entities that are still valid; that is, that have
# not been deleted.
method alive ( --> Int ) {
    my $size = @!entities.elems - 1;
    my $current = $!available;

    until $current.&is-null {
        $size--;
        $current = @!entities[ $current.&entity ];
    }

    $size;
}

# Reset the registry internal storage. All entities will be deleted, and all
# entity IDs will be made available.
method clear ( --> Nil ) {
    %!components = ();
    %!view-cache = ();
    @!entities   = (Mu);
    $!available  = Nil;
}

method !generate-guid ( --> int32 ) {
    die 'Exceeded maximum number of entities' if @!entities.elems >= ENTITY-MASK;
    my int32 $guid = @!entities.elems;
    @!entities.push: $guid;
    $guid;
}

method !recycle-guid ( --> int32 ) {
    die 'Cannot recycle GUID if none has been released' without $!available;

    my int32 $current = $!available;
    my $version = @!entities[$current].&version;
    $!available = @!entities[$current].&entity;

    @!entities[$current] = $current +| ( $version +< ENTITY-SHIFT );
}

# Create a new entity
method create ( *@components --> Int ) {
    my $guid = $!available.&is-null ?? self!generate-guid !! self!recycle-guid;
    $.add: $guid, $_ for @components;
    $guid;
}

method check ( Int() $guid, $c --> Bool() ) {
    .&contains($guid.&entity) with %!components{$c.&type}
}

# Add or replace a component for an entity
multi method add ( Int() $guid, $c --> Nil ) {
    my $e = $guid.&entity;
    my $type = $c.&type;

    #                         SPARSE  DENSE   COMPONENTS
    #                               \   |    /
    given %!components{$type} //= [ [], [], [] ] {
        # Replace component
        if $.check: $guid, $c {
            .[ COMPONENTS; .[ SPARSE; $e ] ] = $c;
        }

        # Add component
        else {
            .[COMPONENTS].push: $c;
            .[DENSE     ].push: $e;

            .[ SPARSE; $e ] = .[DENSE].elems - 1;
        }
    }

    # Adding a component invalidates any cached view that uses it
    %!view-cache{$_}:delete
        for %!view-cache.keys.grep: *.contains("|{ $type.^name }|");
}

# Add or replace a set of components for an entity
multi method add ( $guid, *@c where *.elems > 1 --> Nil ) {
    $.add: $guid, $_ for @c
}

# Get a component for an entity
multi method get ( Int() $guid, $c, :$check = True ) {
    my $e = $guid.&entity;
    return $c.WHAT if $check && !$.check: $guid, $c;

    try { .[ COMPONENTS; .[ SPARSE; $e ] ] } // $c.WHAT
        with %!components{$c.&type};
}

# Get a set of components for an entity
multi method get ( $guid, *@c where *.elems > 1, |c ) {
    @c.map: { $.get: $guid, $_, |c }
}

# Remove an entity and all its components
multi method delete ( Int() $guid --> Nil ) {
    $.delete( $guid, $_ ) for %!components.keys;

    # We mark an entity as available by splitting the entity and the version
    # and storing the incremented version only in the entities list, and the
    # available entity ID in $AVAILABLE

    my $entity  = $guid.&entity;
    my $version = $guid.&version + 1;

    @!entities[$entity] = $!available +| ( $version +< ENTITY-SHIFT );
    $!available = $entity;
}

# Remove a component from an entity
multi method delete ( Int() $guid, $c --> Nil ) {
    return unless $.check: $guid, $c;
    my $e = $guid.&entity;
    my $type = $c.&type;

    with %!components{$type} {
        my ( $i, $j ) = ( .[ SPARSE; $e ], .[DENSE].elems - 1 );

        for .[DENSE], .[COMPONENTS] {
            .[ $i, $j ] = .[ $j, $i ];
            .pop;
        }

        try .[ SPARSE; .[DENSE; $i] // Int ] = $i;
    }

    # Deleting a component invalidates any cached view that uses it
    %!view-cache{$_}:delete
        for %!view-cache.keys.grep: *.contains("|{ $type.^name }|");
}

# Delete a set of components from an entity
multi method delete ( $guid, *@c where *.elems > 1 --> Nil ) {
    @c.map: { $.delete: $guid, $_ }
}

my class View {
    has @!components is built is required;
    has $!view       is built;

    method iterator   { $!view.iterator }
    method entities   { $!view».key     }
    method components { $!view».value   }

    multi method each ( &code where *.cando: \( Int, |@!components ) --> Nil ) {
        code( .key, |.value ) for |$!view;
    }

    multi method each ( &code where *.cando: @!components.Capture --> Nil ) {
        code( |.value ) for |$!view;
    }

    multi method each ( &code --> Nil ) {
        die Q:c:to<FATAL>.chomp;
            The block passed to this view's .each could not be used. To fix this,
            please use a block that can be called with either

                { @!components.Capture.gist }

            to receive the components, or

                { ( Int, |@!components ).Capture.gist }

            to receive the entity and its components
            FATAL
    }
}

# Checks if an entity identifier refers to a valid entity; that is, one that
# has been created and not deleted.
method valid ( Int() $guid --> Bool ) {
    my $pos = $guid.&entity;
    $pos < @!entities.elems && ( @!entities[$pos] // $guid + 1 ) == $guid
}

# Return a view for all entities
# The view of all entities is never cached
multi method view ( Whatever --> View ) {
    View.new(
        components => Empty,
        view => eager @!entities.grep({ $.valid: $_ // 0 }).map: {
            self!add-version(.&entity) => Empty;
        }
    )
}

# Return a view for entities that have the specified set of components
multi method view ( $c --> View ) {
    my $type = $c.&type;
    %!view-cache{ "|$type.^name()|" } //= do {
        my $base  := %!components{$type};
        my $comps := $base[COMPONENTS];

        View.new(
            components => ( $c, ),
            view => eager $base[DENSE].kv.map: -> $i, $e {
                self!add-version($e) => ( $comps[$i]<>, );
            },
        )
    }
}

# Return a view for entities that have the specified set of components
multi method view ( *@c where *.elems > 1 --> View ) {
    my @components = @c.map: { ( .defined ?? .WHAT !! $_ ).&type }

    %!view-cache{ "|@components.map(*.^name).join('|')|" } //= do {
        my ( $short, @rest) = @components.sort: { %!components{$_}.[DENSE].elems }
        my $base  := %!components{$short};
        my $comps := $base[COMPONENTS];

        my @view = eager gather for $base[DENSE].kv -> $i, $e {
            my $guid = self!add-version($e);

            # TODO: This can probably be simplified
            my $not;
            for @rest {
                unless $.check: $guid, $_ {
                    $not = True;
                    last;
                }
            }

            next if $not;

            take $guid => eager @components.map: {
                when $short { $comps[$i]<> }
                default { $.get( $guid, $_, :!check )<> }
            }
        }

        View.new( :@components, :@view )
    }
}

multi method sort ( $c, &comparator ) {
    my $type = $c.&type;
    my $set  = %!components{$type}
        or die "Cannot sort $type: no such component in registry";

    my $sparse := $set[SPARSE];
    my $dense  := $set[DENSE];
    my $comps  := $set[COMPONENTS];

    # Sorting a component invalidates any cached view that uses it
    %!view-cache{$_}:delete
        for %!view-cache.keys.grep: *.contains("|{ $type }|");

    # See https://skypjack.github.io/2019-09-25-ecs-baf-part-5/
    my $sorter = &comparator.count == 2
        ?? { &comparator( $comps[ $sparse[$^a] ], $comps[ $sparse[$^b] ] ) }
        !! { &comparator( $comps[ $sparse[$^a] ] ) }

    $set[DENSE] .= sort: $sorter;

    for ^$dense.elems -> $curr is copy {
        my $next = $sparse[ $dense[ $curr ] ];

        while $next != $curr {
            $set.&swap-components( |$dense[ $curr, $next ] );

            $sparse[ $dense[ $curr ] ] = $curr;
            $curr = $next;
            $next = $sparse[ $dense[ $curr ] ];
        }
    }

    return;
}

multi method sort ( $c, $comparator --> Nil ) {
    my $type = $c.&type;
    my $set  = %!components{ $type }
        or die "Cannot sort { $type }: no such component in registry";

    my $other = %!components{ $comparator.&type }
        or die "Cannot sort according to { $comparator.&type }: no such component in registry";

    # Sorting a component invalidates any cached view that uses it
    %!view-cache{$_}:delete
        for %!view-cache.keys.grep: *.contains("|{ $type }|");

    my $j = 0;
    for $other[DENSE].keys -> $i {
        my $this = $set[   DENSE; $j ] // die "Undefined in set";
        my $that = $other[ DENSE; $i ] // die 'Undefined in other';

        next unless $set.&contains: $that;

        $set.&swap( $this, $that ) unless $this == $that;
        $j++;
    }
}

method ^dump-entities ( \ecs, *@components --> Nil ) {
    my $attr       = ecs.^attributes.first: *.name eq '%!components';
    my $components = $attr.get_value(ecs);

    my @names = @components ?? @components».&type !! $components.keys;

    for @names -> $name {
        with $components{$name} {
            for .[SPARSE].keys -> $i {
                FIRST {
                    say "# [$name.^name()]"
                        if !@components || @components.elems > 1;

                    say '# GUID      SPARSE DENSE WHERE        COMPONENT';
                }

                my $c = .[COMPONENTS; $i].<>;

                say sprintf '# %9s %6s %5s %012X %s',
                    ecs!Game::Entities::add-version($i).&format,
                    .[SPARSE; $i]  // '---',
                    .[DENSE;  $i]  // '---',
                    $c ?? $c.WHERE !! '0',
                    $c ?? $c.gist  !! '---';
            }
        }
    }
}
