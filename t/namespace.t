#!/usr/bin/env raku

use Test;
use Game::Entities;

is-deeply Game::Entities.^methods».name.grep(!(* eq 'BUILDALL' | 'POPULATE')).Set, <
    add
    alive
    check
    clear
    create
    created
    delete
    get
    sort
    valid
    view
>.Set, 'Expected methods in namespace';

done-testing;
