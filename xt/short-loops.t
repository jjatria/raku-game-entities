#!/usr/bin/env raku

use Test;
use Game::Entities;

class A { }
class B { }
class C { }

my \ECS = Game::Entities.new;

subtest 'Baseline' => {
    for 9 -> $n {
        diag "Testing a flat array of $n elements";
        LEAVE diag 'Took ' ~ now - ENTER now;

        my @entities = 0 ...^ $n;

        {
            diag "Testing 10,000 iterations";

            my $elems = 0;

            for ^10_000 {
                my @array = gather for @entities { .take }
                $elems = @array.elems;
                last unless $elems == $n;
            }

            is $elems, $n;

            diag 'Took ' ~ now - ENTER now;
        }
    }
}

subtest 'Short loops' => {
    for 9 -> $n {
        diag "Testing $n entities";
        LEAVE diag 'Took ' ~ now - ENTER now;

        ECS.clear;

        for 0 ..^ $n {
            my $mod = $_ % 3;
            with ECS.create {
                ECS.add: $_, A.new;
                ECS.add: $_, B.new if $mod;
                ECS.add: $_, C.new if $mod == 2;
            }
        }

        for (
                $n     => ( A, ),
            2 * $n / 3 => ( A, B, ),
                $n / 3 => ( B, C, ),
                $n / 3 => ( A, C, ),
                $n / 3 => ( A, B, C, ),
        ) -> ( :key($want), :value(@components) ) {
            diag "Testing 10,000 iterations of { join '-', @components.map(*.^name) }";

            my $elems = 0;

            for ^10_000 {
                my @array = gather for ECS.view(|@components) { .take }
                $elems = @array.elems;
                last unless $elems == $want;
            }

            is $elems, $want;

            diag 'Took ' ~ now - ENTER now;
        }
    }
}

done-testing;
